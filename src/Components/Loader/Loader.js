import React from 'react';

import logo from '../../assets/icons/logo-icon.svg';

import './Loader.css'


const Loader = () =>
  (
    <div className="loader">
      <img className="loader__image" src={logo} alt=""/>
      <p>Chargement des films...</p>
    </div>
  )

export default Loader