import React from 'react';

import MovieItem from './MovieItem/MovieItem'
import Loader from '../Loader/Loader'
import GenericError from '../GenericError/GenericError'

import useFetch from '../../Hooks/useFetch'

import { MOVIES_LIST_URL } from '../../static/Constants'

const MoviesList = () => {
  const { response, isLoading } = useFetch(MOVIES_LIST_URL)
  if (!isLoading) {
    if (response !== null && response.data !== null) {
      const { data: movies } = response
      const list = movies.map(({ id, titre, directeur, acteurs, dateSortie, resume })  => {
        const listActors = acteurs.join(', ')
        return (
          <MovieItem 
            key={id} 
            id={id} 
            title={titre} 
            director={directeur} 
            actors={listActors} 
            releaseYear={dateSortie} 
            plot={resume} 
          />
        )
      })
      return (
        <div className="movies-list">
          {list}
        </div>
      )
    } else {
      return (
        <GenericError />
      )
    }
  }
  return (
    <Loader />
  )
}

export default MoviesList