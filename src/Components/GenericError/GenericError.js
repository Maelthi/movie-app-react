import React from 'react';

import error from '../../assets/icons/error-icon.svg';


import './GenericError.css'

const GenericError = () => 
    (
        <div className="generic-error">
            <img className="generic-error__img" src={error} alt="coucou" />
            <p>Nous ne sommes pas en mesure d'afficher les films, mais pas d'inquiétude, le détective Philip Marlowe (<i>Le Grand Sommeil</i>) est sur la piste...</p>
        </div>
    )


export default GenericError