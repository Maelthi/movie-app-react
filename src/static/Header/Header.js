import React from 'react';

import SearchBar from '../../Components/SearchBar/SearchBar'

import logo from '../../assets/icons/logo-icon.svg';

import './Header.css'

const Header = () => {

  const handleChange = value => {
    console.log(value)
  }
  
  return (
    <>
      <div className='header'>
        <img className="header__logo" src={logo} alt=""/>
        <h1 className="header__title">Movies</h1>
      </div>
      <SearchBar handleChange={handleChange} />
    </>
  )
}

export default Header