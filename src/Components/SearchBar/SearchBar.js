import React, { useState } from 'react';

import './SearchBar.css'

import search from '../../assets/icons/search-icon.svg';
import close from '../../assets/icons/close-icon.svg';

const SearchBar = handleChange => {

  const placeholder = '"Casablanca", "Paul Newman"'
  const [exampleSearch, setExampleSearch] = useState(placeholder)
  const [closeIconDisplayed, setCloseIconDisplayed] = useState(false)

  const searchInput = document.getElementById('search-bar__input')

  const actionsOnCloseClicked = () => {
    searchInput.value= ''
    searchInput.focus()
    setCloseIconDisplayed(false)
  }

  const actionsOnChangeInput = (e) => {
    setCloseIconDisplayed(true)
    const valueInput = e.target.value
    if (valueInput.length === 0) {
      setExampleSearch(placeholder)
      setCloseIconDisplayed(false)
    }
  }

  return (
    <div className="search-bar" onClick={() => setExampleSearch('')}>
      <input id="search-bar__input" className="search-bar__input" type="text" placeholder={exampleSearch} onChange={e => actionsOnChangeInput(e)}/>
      <img className="search-bar__icon" src={closeIconDisplayed ? close : search} onClick={closeIconDisplayed ? actionsOnCloseClicked : null} alt="" />
    </div>
  )
}

export default SearchBar