import React from 'react';
import Header from './static/Header/Header'

import MoviesList from './Components/MoviesList/MoviesList';

import './Components/MoviesList/MovieItem/MovieItem.css'

const Movies = () => {

  return(
    <div className="movies-container">
      <Header />
      <MoviesList /> 
    </div>
  )
}

export default Movies;
