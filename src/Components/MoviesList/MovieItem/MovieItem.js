import React, { useState } from 'react';
import PropTypes from 'prop-types'

import arrow from '../../../assets/icons/arrow-icon.svg';

import './MovieItem.css'

const MovieItem = ({ title, director, releaseYear, actors, plot }) => { 

  const substractPlot = plot => {
    return plot.substr(0, 250) + '...'
  }
  
  let [shortPlot, setShortPlot] = useState(substractPlot(plot))
  let [plotLengthBtn, setplotLengthBtn] = useState('Lire plus')
  let [arrowDown, setArrowDown] = useState(true)

  const handleClick = () => {
    if (shortPlot.length === plot.length) {
      setShortPlot(substractPlot(plot))
      setplotLengthBtn('Lire plus')
      setArrowDown(true)
    } else {
      setShortPlot(shortPlot = plot)  
      setplotLengthBtn('Lire moins')
      setArrowDown(false)
    }
  }

  return (
  <div className="movie-item" onClick={handleClick}>
    <div className="movie-item__header">
    <h2 className="movie-item__title">{title}</h2>
    </div>
    <p>{director} - {releaseYear}</p>
    <p>Avec {actors}</p>
    <p className={`movie-item__plot movie-item__plot${arrowDown === false ? '--full' : ''}`}>{shortPlot}</p>
    <div className="movie-item__more">
      <img className={`movie-item__arrow ${arrowDown === false ? 'movie-item__arrow--up' : ''}`} src={arrow} alt=""/>
      <p className="movie-item__read-more">{plotLengthBtn}</p>
    </div>
  </div>
  )
}

MovieItem.propTypes = {
  title: PropTypes.string.isRequired,
  director: PropTypes.string.isRequired, 
  releaseYear: PropTypes.number, 
  actors: PropTypes.string.isRequired, 
  plot: PropTypes.string.isRequired
}


export default MovieItem