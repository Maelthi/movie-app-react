import { useEffect, useState } from 'react';
import axios from 'axios'

const useFetch = url => {
  const [response, setResponse] = useState(null)
  const [error, setError] = useState(null)
  const [isLoading, setIsLoading] = useState(false)

  useEffect (() => {

    const fetchData = async () => {
      setIsLoading(true)
      try {
        const res = await axios.get(url)
        setIsLoading(false)
        setTimeout(() => {
          setResponse(res)
        }, 400)
      } catch (error) {
        setError(error)
        setIsLoading(false)
      }
    }
    fetchData()
  }, [])
  return {response, isLoading, error}
}

export default useFetch